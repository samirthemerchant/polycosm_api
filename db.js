const fs = require( 'fs' )
const ini = require( 'ini' )
const { Pool } = require( 'pg' )

const config = ini.parse( fs.readFileSync( '/private/rsahub/config.ini', 'utf-8' ) )

const pool = new Pool({
    user: config.postgres.username,
    password: config.postgres.password,
    database: config.postgres.database,
    host: config.postgres.host,
    port: config.postgres.port
})

module.exports = {
    query: ( text, params ) => pool.query( text, params )
}