const express = require( 'express' )
const bodyParser = require( 'body-parser' )
const crypto = require( 'crypto' )
const fs = require( 'fs' )
const ini = require( 'ini' )

const db = require( './db' )

const app = express()
const config = ini.parse( fs.readFileSync( '/private/rsahub/config.ini', 'utf-8' ) )

app.use( bodyParser.json() )
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

app.post( '/hubs/amend', async ( req, res ) => {
    const { account_id, hub_sid, token } = req.body
    if( ! account_id || ! hub_sid || ! token ){
        res.status( 500 ).send( 'Missing field' )
        return
    }

    try {
        const hmac = crypto.createHmac( 'sha512', config.api.secret )
                            .update( `${hub_sid}_${account_id}` )
                            .digest( 'hex' )
        if( token !== hmac ) {
            res.status( 401 ).send( 'Not authorised' )
            return
        }
        
        const query = 'UPDATE ret0.hubs SET created_by_account_id = $1, creator_assignment_token = $2, member_permissions = $3 WHERE hub_sid = $4'
        const result = await db.query( query, [ account_id, '', 16, hub_sid ] )
        res.status( 200 ).send( result )
    } catch( e ) {
        res.status( 500 ).send( e )
    }

    return
})

app.get( '/hubs/active', async ( req, res ) => {
    const { gambit, token } = req.query
    if( ! gambit || ! token ) {
        res.status( 500 ).send( 'Missing field' )
        return
    }

    try {

        const hmac = crypto.createHmac( 'sha512', config.api.secret )
        .update( gambit )
        .digest( 'hex' )
        if( token !== hmac ) {
            res.status( 401 ).send( 'Not authorised' )
            return
        }
        
        const query = 'SELECT hub_sid FROM ret0.hubs WHERE entry_mode = $1'
        const result = await db.query( query, [ 'allow' ] )
        res.status( 200 ).send( result.rows )
    } catch( e ) {
        console.log( e ) 
        res.status( 500 ).send( e )
    }
    return
})
    
app.listen(config.api.port, () => {
    console.log(`App running on port ${config.api.port}.`)
})